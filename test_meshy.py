#!/usr/bin/env python
# -*- Encoding: utf-8 -*-
#
#  Python interface to the Serval mesh software
#
#  Copyright 2015-2016 Kevin Steen <ks@kevinsteen.net>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

'''Tests for meshy.py

Tests which require a disk folder (e.g. for a serval instance) will create a
folder under 'TEST_BASE_DIR' named after the test (class/testname).
'''
from __future__ import print_function, unicode_literals, division

import unittest

import logging

from meshy import *


TEST_BASE_DIR = '/tmp/ramdisk/mtst'
# # mkdir /tmp/ramdisk
# # mount -t tmpfs -o size=5000k tmpfs /tmp/ramdisk

logging.basicConfig(
                format='%(asctime)s:%(name)s:%(levelname)s:%(message)s',
                datefmt = '%H:%M:%S',
                level=logging.DEBUG)


def setUpModule():
    import shutil
    shutil.rmtree(TEST_BASE_DIR, ignore_errors=True)


def make_test_bundle(uniq):
    b = Bundle(
        version=201601, service='meshy_test', name='talk,test',
        testbundle=uniq
        )
    b.payload = bytearray([uniq] + list(range(256)))
    return b


class Base(unittest.TestCase):
    '''So I don't have to remember the CamelCase'''

class ServaldRequired(Base):
    '''Provides a new Servald instance as self.servald'''
    API_PORT = 4777
    def setUp(self): # Make sure daemon is running
        try:
            tstfile, tstclass, tstname = self.id().split('.')
            testdir = os.path.abspath(os.path.join(
                                         TEST_BASE_DIR, tstclass, tstname))
            print('Test directory:', testdir, '(len:%s)' % len(testdir))
            self.servald = Servald(instancepath=testdir)
            self.servald.config_update({
                'debug.verbose': 'true',
                'log.console.level': 'debug',
                'rhizome.min_free_space': '10000',
                'rhizome.http.port': str(self.API_PORT),  # Don't clash with running
                })
            self.servald.start()
        except Exception:
            self.tearDown()
            raise

    def tearDown(self):
        try:
            self.servald.stop_running_daemon()
        except Exception:
            pass



class RhizomeRequired(ServaldRequired):
    '''Provides a Rhizome object as self.r'''
    def setUp(self): # Make sure daemon is running
        try:
            super(RhizomeRequired, self).setUp()
            self.r = self.servald.rhizome
        except Exception:
            self.tearDown()
            raise

    def tearDown(self):
        super(RhizomeRequired, self).tearDown()



class StandaloneTests(Base):
    def test_matches_template_good(self):
        import meshy as m
        self.assertTrue(m._matches_template(
            {'a':1, 'b':'two', 'c':[3]}, {'a':1}))
        self.assertTrue(m._matches_template(
            {'a':1, 'b':'two', 'c':[3]}, {'a':1, 'b':'two'}))
        self.assertTrue(m._matches_template(
            {'a':1, 'b':'two', 'c':[3]}, {'a':1, 'b':'two', 'c':[3]}))

    def test_matches_template_invalid_field(self):
        import meshy as m
        self.assertRaises(KeyError, m._matches_template,
            {'a': 1, 'b': 'two', 'c': [3]},
            {'d': 'two'}
            )

    def test_matches_template_bad(self):
        import meshy as m
        self.assertFalse(m._matches_template(
            {'a':1, 'b':'two', 'c':[3]}, {'a':2}))
        self.assertFalse(m._matches_template(
            {'a':1, 'b':'two', 'c':[3]}, {'a':1, 'b':'wo'}))
        self.assertFalse(m._matches_template(
            {'a':1, 'b':'two', 'c':[3]}, {'c':3}))

    def test_bundle_non_manifest_keys_removed(self):
        b = Bundle(payload='some data', secret='hideme')
        manifest = b.get_unsigned_manifest()
        #Ensure no false-positives from unicode/bytes:
        self.assertNotIn(b'payload', manifest)
        self.assertNotIn(b'secret', manifest)

    def test_bundle_unset_keys_missing(self):
        b = Bundle()
        self.assertRaises(AttributeError, lambda x: x.missing, b)
        self.assertRaises(KeyError, lambda x: x['unknown'], b)

    def test_bundle_update_from_headers_lowercases_keys(self):
        '''When updating from headers, ensure that keys created from the
        headers are lowercase'''
        bundle = Bundle()
        bundle.update_from_headers([('Serval-Rhizome-Bundle-Baloney', 5)])
        self.assertIn('baloney', bundle)
        self.assertEqual(bundle['baloney'], 5)
        self.assertEqual(bundle.baloney, 5)

    def test_bundle_init_equivalent(self):
        a = Bundle({'attr': 5, 'payload': 'TestData'})
        b = Bundle(payload='TestData', attr=5)
        c = Bundle()
        c.payload = 'TestData'
        c.attr = 5
        self.assertEqual(b.attr, 5)
        self.assertEqual(c.attr, b.attr)
        self.assertEqual(b.payload, 'TestData')
        self.assertEqual(c.payload, b.payload)
        self.assertEqual(b, a)
        self.assertEqual(c, b)

    def test_RhizomeResult_decoding(self):
        headers = [
            ('Serval-Rhizome-Result-Bundle-status-code', '5'),
            ('serval-rhizome-result-bundle-status-message', 'Bundle new to store'),
            ('serval-rhizome-result-payload-status-code', 1),
            ('serval-rhizome-result-payload-status-message', 'Payload new to store'),
            ]
        res = RhizomeResult(http_status_code='200', headers=headers)
        self.assertEqual(res.http_status_code, 200)
        res = RhizomeResult(http_status_code=200, headers=headers)
        self.assertEqual(res.http_status_code, 200)
        self.assertEqual(res.bundle_status_code, 5)
        self.assertEqual(res.payload_status_code, 1)

    def test_decode_rhizome_null_stream(self):
        import meshy
        import io
        stream = io.StringIO('')
        with self.assertRaises(ValueError):
            next(meshy._decode_json(stream))

    def test_decode_rhizome_empty_stream(self):
        import meshy
        import io
        stream = ('{\n'
            '"header":[".token","_id","service","id","version","date",'
            '".inserttime",".author",".fromhere","filesize","filehash",'
            '"sender","recipient","name"],\n'
            '"rows":[\n'
            ']\n'
            '}\n')
        stream = io.StringIO(stream)
        headers, rows = meshy._decode_json(stream)
        self.assertEqual(headers, {})
        with self.assertRaises(StopIteration):
            next(rows)

    def test_decode_rhizome_single_item_stream(self):
        import meshy
        import io
        stream = ('{\n'
            '"header":[".token","_id","service","id","version","date",".inserttime",".author",".fromhere","filesize","filehash","sender","recipient","name"],\n'
            '"rows":[\n'
            '["-_n-t1ILRqOYYUa7vEgEKAEAAAAAAAAA",1,"meshy","0E870608ABBD1CD510D00E5CBEBC4513A703B5712CEE6FC65F9296281D757363",1,1450395782194,1450395782627,null,0,256,"1E7B80BC8EDC552C8FEEB2780E111477E5BC70465FAC1A77B29B35980C3F0CE4A036A6C9462036824BD56801E62AF7E9FEBA5C22ED8A5AF877BF7DE117DCAC6D",null,null,"talk"]\n'
            ']\n'
            '}\n')
        stream = io.StringIO(stream)
        headers, rows = meshy._decode_json(stream)
        self.assertEqual(headers, {})
        items = list(rows)
        self.assertEqual(len(items), 1)

    def test_decode_json_functional(self):
        import meshy
        import io
        stream = (u'{\n'
            u'"header":[".token","_id"],\n'
            u'"rows":[\n'
            u'["tokenvalue", 2]\n'
            u']\n'
            u'}\n')
        stream = io.StringIO(stream)
        headers, rows = meshy._decode_json(stream)
        self.assertEqual(headers, {})
        decoded = next(rows)
        self.assertEqual(decoded, {'.token': "tokenvalue", '_id': 2})

    def test_decode_json_with_prefixheaders(self):
        import meshy
        import io
        stream = (u'{\n'
            u'"read_offset":0,\n'
            u'"latest_ack_offset":1,\n'
            u'"header":["type","my_sid","their_sid"],\n'
            u'"rows":[\n'
            u'["<","5C960","ACCEA"]\n'
            u'["<","LAST1","LAST2"]\n'
            u']\n'
            u'\n}')
        stream = io.StringIO(stream)
        headers, rows = meshy._decode_json(stream)
        self.assertEqual(headers, {'read_offset':0, "latest_ack_offset":1})
        row = next(rows)
        self.assertEqual(row, {'type':'<', 'my_sid':'5C960', 'their_sid':'ACCEA'})
        row = next(rows)
        self.assertEqual(row, {'type':'<', 'my_sid':'LAST1', 'their_sid':'LAST2'})




class REST_API_Tests(RhizomeRequired):
    def test_post_simple(self):
        api = REST_API(('test','testpass'), port=self.API_PORT)
        params = [
            ('manifest', bytearray(b'version=1\nname=talk\nservice=meshy\n'),
                'rhizome/manifest; format=text+binarysig'),
            ('payload', bytearray(b'blahblah'), 'application/octet-stream'),
            ]
        res = api.post_bundle(path='rhizome/insert', params=params)
        self.assertEqual(res.status, 201)

    def test_post_empty_payload(self):
        api = REST_API(('test','testpass'), port=self.API_PORT)
        params = [
            ('manifest', bytearray(b'version=1\nname=talk\nservice=meshy\n'),
                'rhizome/manifest; format=text+binarysig'),
            ('payload', None, 'application/octet-stream'),
            ]
        res = api.post_bundle(path='rhizome/insert', params=params)
        self.assertEqual(res.status, 201)

    def test_get_json_list(self):
        result = self.r._api.GET_json_simple('keyring/identities.json')
        assert 'header' in result
        assert 'rows' in result

    def test_get_json_simple(self):
        result = self.r._api.GET_json_simple('keyring/add')
        assert 'sid' in result['identity']



class RhizomeFilter(RhizomeRequired):
    def test_empty_filter(self):
        b = make_test_bundle(1)
        self.r.insert_bundle(b)
        self.assertTrue(len(self.r.bundles)==1)
        self.assertTrue(len(list(self.r.find_bundles()))==1)
        b = make_test_bundle(2)
        self.r.insert_bundle(b)
        self.assertTrue(len(self.r.bundles)==2)
        self.assertTrue(len(list(self.r.find_bundles()))==2)
        self.assertEqual(list(self.r.bundles), list(self.r.find_bundles()))

    def test_service_filter(self):
        self.assertTrue(len(list(self.r.find_bundles()))==0)
        for uniq in [1, 2, 3]:
            b = make_test_bundle(uniq)
            b.service = 'meshytestservice' + str(uniq)
            self.r.insert_bundle(b)
        self.assertEqual(len(list(
            self.r.find_bundles(service='meshytestservice2'))), 1)



class RhizomeTests(RhizomeRequired):
    def test_no_error_on_empty_rhizome_store(self):
        '''Make sure we're testing against an empty rhizome store'''
        bundlelist = list(self.r.find_bundles())
        self.assertEqual(len(bundlelist), 0)

    def test_empty_payload(self):
        b = make_test_bundle(2)
        b.payload = None
        self.r.insert_bundle(b)
        self.assertIsNone(b.payload)

    def test_entire_bundle_saved(self):
        '''Arbitrary fields are preserved by insert and retrieve'''
        b = Bundle()
        data = bytearray(range(256))
        b.payload = data
        b.service = 'testservice'
        b.name = 'testname'
        b.randomattribute = 'randomattrvalue'
        b['randomkey'] = 'randomkeyvalue'
        b.version = 53
        self.r.insert_bundle(b)
        c = Bundle(id=b.id)
        newb = self.r.get_bundle_manifest(c)
        self.assertEqual(newb.service, 'testservice')
        self.assertEqual(newb.name, 'testname')
        self.assertEqual(newb.randomattribute, 'randomattrvalue')
        self.assertEqual(newb.randomkey, 'randomkeyvalue')
        self.assertEqual(int(newb.version), 53)
        newc = self.r.get_bundle_payload_raw(c)
        self.assertEqual(newc.payload, data)

    def test_text_payload(self):
        data = (b'Date: Thu, 03 Dec 2015 17:34:47 +0000\r\n'
            b'From: News1 Test <news1@localhost>\r\n'
            b'User-Agent: Mozilla/5.0 (X11; Linux i686; rv:31.0) Gecko/20100101'
            b' Icedove/31.8.0\r\nMIME-Version: 1.0\r\nNewsgroups: talk\r\n'
            b'Subject: test2\r\nContent-Type: text/plain; charset=utf-8\r\n'
            b'Content-Transfer-Encoding: 7bit\r\n\r\ntest2 message\r\n')
        b = Bundle(service='meshforum', payload=data, forums='test')
        self.r.insert_bundle(b)
        c = Bundle(id=b.id)
        self.r.get_bundle_payload_raw(c)
        self.assertEqual(c.payload, data)

    def test_unicode_filenames_ok(self):
        #~ test_strings = ['Hello Montreal!', '¡‫ן‬ɐǝɹʇuoɯ o‫ןן‬ǝɥ',
            #~ 'ђєɭɭ๏ ๓๏ภՇгєค !', 'Hi ℙƴ☂ℌøἤ']
        b = Bundle()
        b.service = 'file'
        b.name = u'Hi ℙƴ☂ℌøἤ'
        self.r.insert_bundle(b)
        c = Bundle(id=b.id)
        d = self.r.get_bundle_manifest(c)
        self.assertEqual(d.name, b.name)

    def test_insert_bundle_basic(self):
        inserted = make_test_bundle(1)
        self.r.insert_bundle(inserted)
        self.assertIn('secret', inserted)
        retrieved = self.r.get_bundle_manifest(Bundle(id=inserted.id))
        retrieved = self.r.get_bundle_payload_raw(retrieved)
        del inserted['secret']
        self.assertEqual(inserted, retrieved)

    def test_insert_bundle_with_file(self):
        with open('test_meshy.py','rb') as f:
            inserted = Bundle(service='file', payload=f, name='test_meshy')
            res = self.r.insert_bundle(inserted)
            self.assertEqual(res.http_status_code, 201)

    def test_insert_bundle_with_dict(self):
        '''Supply a dictionary instead of a bundle to insert_bundle'''
        inserted = dict(myfield='myvalue')
        self.assertRaises(AttributeError, self.r.insert_bundle, inserted)

    def test_rest_get_ok_on_empty_rhizome_store(self):
        api = REST_API(auth=self.r.auth, port=self.r.RESTport)
        with self.assertRaises(StopIteration):
            response = next(api.GET_json_list(path='rhizome/bundlelist.json'))

    def test_append_bundle(self):
        bob = make_test_bundle(1)
        self.r.bundles.append(bob)
        c = Bundle(id=bob.id)
        d = self.r.get_bundle_manifest(c)
        self.assertEqual(d.id, bob.id)



class ServaldStandaloneTests(Base):
    # TODO
    def test_interactive_empty_warning(self):
        '''Running d=Servald() should give a warning'''
        self.assertRaises(ServalError, Servald)

    def test_parse_monitor_no_terminator(self):
        buf = b'Some data with no LF'
        eventlist, nbuf = Servald.parse_monitor_stream(buf)
        assert nbuf == buf

    def test_parse_monitor_newpeer(self):
        buf = b'NEWPEER:ABCDEF\n'
        eventlist, nbuf = Servald.parse_monitor_stream(buf)
        assert ('NEWPEER', u'ABCDEF') == eventlist[0]

    def test_parse_monitor_unknown(self):
        buf = b'Blarney\n'
        eventlist, nbuf = Servald.parse_monitor_stream(buf)
        assert [('UNKNOWN', buf[:-1])] == eventlist

    def test_parse_monitor_bundle(self):
        #~ Monitor received:'\n*377:BUNDLE:66E0B8699C187BDAB55B19CC73652A221083E3A070A6E80BF986EA44CDB89938\nid=66E0B8699C187BDAB55B19CC73652A221083E3A070A6E80BF986EA44CDB89938\nservice=private\ncrypt=1\nversion=19\ndate=1495125793637\ntail=0\nfilesize=19\nfilehash=29956C7FE2258E5FF9AF2674BBC1A295BC2169A3AE7D826866F190ECA3DFF4EB800A9A98FB3AF9637BB45C7C73AE37FA7BE28FC59C8124ED5D9AB34033B30B01\n\x00\x17\xa6\x18\xde\xd1\x82\x1f\xfb\xfa\xd6,K\x08\xf1\xd6\x11@\xce\\\x19\x00\x02\xfcL\xacA\x8dl:\x9c\xde\xcf\x95\xf3\xa4Jd;!\xa0T\xf7\xf5\x8d\x8cd\xd3U\x9c\x0c\xd2&!\xc3\xa5^\xc1\x92\xaf\xd5nZ\xc2.\x0ef\xe0\xb8i\x9c\x18{\xda\xb5[\x19\xccse*"\x10\x83\xe3\xa0p\xa6\xe8\x0b\xf9\x86\xeaD\xcd\xb8\x998\n'
        #~ 18:08:42:meshygui:DEBUG:NEED TO READ SOME BYTES FROM MONITOR
        #~ Monitor received:'\n*449:BUNDLE:F92DF840BC73D63E3643AF80F06AD596791E9F97C78B1B8B21CFFA44E0D336A4\nname=jo\nservice=MeshMB1\nid=F92DF840BC73D63E3643AF80F06AD596791E9F97C78B1B8B21CFFA44E0D336A4\nsender=B9D108AF1AF42A2F0E5A64B4CFD028EA03EE1526357AB85CD5669323DB2B0449\nfilesize=27\ntail=0\nversion=27\ndate=1495125793505\nfilehash=29E4B2209799068EA37AA964C50C534979C0E2A14883C3413CBE724341E8B513B9F5D44A49A337B3B3F2F04FB9904796030F55F165151A3BAFC6FE11643BC6F8\n\x00\x17\xa3\x13\xa4\x97\xf3y3\xa1\x85\xaa\x8a\xaf\xd4\xabP\xc9\xa4.\xeb7\x14\x1b\xe4N\xf28\x1d\x8bY\xef\x8bh!<\xbc\x97u\xc8\xd3\xcc\xe6\xca,\xc5{\x7f-\xdd12\xfc\x89<Z\xb2UF\xcdCw\xbf\xbd:\x0f\xf9-\xf8@\xbcs\xd6>6C\xaf\x80\xf0j\xd5\x96y'
        buf = b'\n*377:BUNDLE:66E0B8699C187BDAB55B19CC73652A221083E3A070A6E80BF986EA44CDB89938\nid=66E0B8699C187BDAB55B19CC73652A221083E3A070A6E80BF986EA44CDB89938\nservice=private\ncrypt=1\nversion=19\ndate=1495125793637\ntail=0\nfilesize=19\nfilehash=29956C7FE2258E5FF9AF2674BBC1A295BC2169A3AE7D826866F190ECA3DFF4EB800A9A98FB3AF9637BB45C7C73AE37FA7BE28FC59C8124ED5D9AB34033B30B01\n\x00\x17\xa6\x18\xde\xd1\x82\x1f\xfb\xfa\xd6,K\x08\xf1\xd6\x11@\xce\\\x19\x00\x02\xfcL\xacA\x8dl:\x9c\xde\xcf\x95\xf3\xa4Jd;!\xa0T\xf7\xf5\x8d\x8cd\xd3U\x9c\x0c\xd2&!\xc3\xa5^\xc1\x92\xaf\xd5nZ\xc2.\x0ef\xe0\xb8i\x9c\x18{\xda\xb5[\x19\xccse*"\x10\x83\xe3\xa0p\xa6\xe8\x0b\xf9\x86\xeaD\xcd\xb8\x998\nExtra Line\nSecond Line\n'
        eventlist, buf = Servald.parse_monitor_stream(buf)
        assert 'BINARY' == eventlist[0][0]
        assert b'' == buf



class ServaldTests(ServaldRequired):
    def test_simple_startstop(self):
        # Work already done in .setUp
        self.assertTrue(self.servald)

    def test_keyring(self):
        # A running servald should have at least 1 identity
        k = self.servald.keyring
        assert 1 == len(k)

    def test_keyring_add(self):
        api = self.servald.rhizome._api
        #~ print(list(api.GET('keyring/identities.json')))
        keyring = self.servald.keyring
        new_sid = keyring.create_SID()
        assert isinstance(new_sid, SID)



class ReadmeTests(RhizomeRequired):
    '''Tests for the examples in the README'''
    def test_example1(self):
        import meshy
        with meshy.start_servald('run') as servald:
            bundle = meshy.Bundle(name='README', payload=b'Contents')
            result = servald.rhizome.insert_bundle(bundle)
            print(bundle.id, bundle.version)

    def test_example2(self):
        import meshy
        with meshy.start_servald('run') as servald:
            for bundle in servald.rhizome:
                print(bundle.service, bundle.filesize)

    #Example 3 is tested by example2

if __name__ == '__main__':
    unittest.main()
