
meshy: Python interface to the Serval mesh software
======================================================

Version: 0.2.0
October 2016


Introduction
-------------

meshy provides a simplified _'Pythonic'_ interface to the Serval
communication daemon from the [Serval Project]. It currently allows
starting/stopping the daemon and using the REST API to insert and
retrieve Bundles. The code is licensed under GNU AGPL 3+ and runs on
Python 2.7 and 3 with no other dependencies.

All advice on how to improve the library will be gratefully received,
as will code reviews, bug reports, patches and any other contributions.

[Serval Project]: http://www.servalproject.org/


Usage
------

### Example 1: Inserting data into Rhizome storage

        import meshy
        with meshy.start_servald('run') as servald:
            bundle = meshy.Bundle(name='README', payload=b'Contents')
            result = servald.rhizome.insert_bundle(bundle)
            print(bundle.id, bundle.version)

### Example 2: List everything in Rhizome

        import meshy
        with meshy.start_servald('run') as servald:
            for bundle in servald.rhizome:
                print(bundle.service, bundle.filesize)

### Example 3:
Here we're going to run an isolated instance in the directory
`~/runserval` and the serval binary is in directory `serval-dna`

    import meshy
    servald = meshy.start_servald(instancepath='~/runserval',
                                  binpath='serval-dna/servald')

See the module and class docstrings for more detail.


Install
--------

1. Copy `meshy.py` into the same folder as your project, or place
   it in the standard location for python modules on your system.


Contact
--------

Code : <https://github.com/skyguy/meshylib>

I'm subscribed to the <serval-project-developers@googlegroups.com>
mailing list, so I should see any messages posted there, or email me at
<meshy@kevinsteen.net>


License
--------

Copyright 2015-2016 Kevin Steen <ks@kevinsteen.net>

Unless otherwise indicated, all source code is Free Software. You can
redistribute it and/or modify it under the terms of the GNU Affero
General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.
A copy of the license can be found in the file `COPYING`.

---

This file is licensed under the Creative Commons Attribution-ShareAlike
4.0 International License. To view a copy of this license, visit
<http://creativecommons.org/licenses/by-sa/4.0/>
