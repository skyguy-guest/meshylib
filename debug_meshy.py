#!/usr/bin/env python
# -*- Encoding: utf-8 -*-
#
# Used to test & debug serval.py

from __future__ import print_function, unicode_literals, division

import logging
logging.basicConfig(
                format='%(asctime)s:%(name)s:%(levelname)s:%(message)s',
                datefmt = '%H:%M:%S',
                level=logging.DEBUG,
                )
#~ # create console handler and set level to debug
#~ console = logging.StreamHandler()
#~ console.setLevel(logging.INFO)
#~ # create formatter
#~ #formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
#~ # add formatter to ch
#~ #console.setFormatter(formatter)
#~ # add ch to logger
#~ logging.getLogger('').addHandler(console)

from meshy import *

def main():

    #~ import meshy, io
    #~ res = meshy._decode_rhizome_json_list(io.StringIO('\n'))
    #~ print(next(res))
    #~ return

    #~ import tempfile
    #~ tempdir = tempfile.mkdtemp(dir='/tmp', suffix='meshytst')
    #~ servald = Servald(instancepath=tempdir)
    #~ servald.config_set('api.restful.users.test.password', 'testpass')
    #~ servald.start()

    servald = start_servald(instancepath='~/runserval')
    try:
        print(servald.keyring)
        print(servald.get_rhizome())
        print(servald.get_rhizome()._api)
        #~ b = Bundle({'defstr':'defstr',
                    #~ u'ustr':u'ustr',
                    #~ }, keyword='keyword')
        #~ b.prop = 'prop'
        #~ print(b)
        #~ print(repr(b))
        #~ print(str(b))
        #~ return
        #~ r = servald.get_rhizome()
        #~ raw = list(r._api.GET_json_list('rhizome/bundlelist.json'))
        #~ logging.getLogger('').setLevel(logging.DEBUG)
        #~ import sys
        #~ for b in list(r._filter_bundles(raw, {}))[:5]:
        #~ #for b in r._filter_bundles(raw, {'service':'meshy_test','name':'talk'}):
            #~ logd('%r',b)
            #~ #logd('%s',b)
            #~ print('Summmary:', b.summary)
            #~ #print('b:', b.encode('utf8')) #fail
            #~ print(b.__str__().encode('utf8'))
            #~ sys.stdout.flush()
            #~ logging.getLogger('').info('------------------------------')
        #~ logd('d msg')
        #~ logd('d msg 1:%s', 1)
        #~ #s = start_test_servald(binpath='runserval/servald')
        #~ #s.stop()
        #~ #s = start_servald(instancepath='./dooby')
        #~ #s = start_test_servald()

        #~ r = Rhizome('test','testpass')#, 'http://localhost:2000/')
        #~ #print(r.SIDlist)

        #~ #print(r.bundles)
        #~ print('EMPTY LIST:',*r.get_bundles())
        #~ b = Bundle(
            #~ version=1, service='meshy',
            #~ #name=u'talk\u2603\u2620',
            #~ name=u'talk',
            #~ forums='talk,test'#,
            #~ #author = r.SIDlist[0]['sid']
            #~ )
        #~ b.payload = bytearray(list(range(256)))
        #~ print('ABOUT TO INSERT:', b)
        #~ res = r.insert_bundle(b)
        #~ print('RESULT:', res)
        #~ print('ONE ITEM:', *r.get_bundles())
    finally:
        servald.stop_running_daemon()
    return

    #~ print('INSERTED BUNDLE:', b)
    #~ c = Bundle(id=b.id)
    #~ print(c)
    #~ d = r.get_bundle_manifest(c)
    #~ print('FULL BUNDLE:', d)
    #~ print('b == d :', b == d)
    #~ e = r.get_bundle_payload_raw(d)
    #~ print('e PAYLOAD:', e.payload)
    #~ return

    #a = Daemon('runserval', 'serval-dna/servald')
    #print('RETURNED:', a.start())
    #print('RETURNED:', a.stop())
    #return

    #~ api = Serval_REST_API('test','testpass')
    #~ from StringIO import StringIO
    #~ b = Bundle(
        #~ version=1, service='serval.py', name='talk',
        #~ forums='talk,test'
        #~ )
    #~ b.payload=StringIO(bytearray(i for i in range(20)))
    #~ print(b)
    #~ r = api.post_bundle(b)
    #~ print(b)
    #~ return

    #~ r = Rhizome('test','testpass')#, 'http://localhost:2000/')
    #~ b = Bundle(
        #~ version=1, service='serval.py',
        #~ #name=u'talk\u2603\u2620',
        #~ name=u'talk',
        #~ forums='talk,test'#,
        #~ #author = r.SIDlist[0]['sid']
        #~ )
    #~ b.payload = ''.join(chr(i) for i in range(22))
    #~ r.insert_bundle(b)
    #~ print(b)
    #~ return

    #~ r = Rhizome('test','testpass')
    #~ b = Bundle(
        #~ version=1, service='serval.py', name='talk',
        #~ forums='talk,test'
        #~ )
    #~ b.payload = ''.join(chr(i) for i in range(22))
    #~ print('before:', b)
    #~ rb = r.bundles
    #~ rb.append(b)
    #~ print('after:', b)

    #~ try:
        #~ print(r.SIDlist)
    #~ except IOError:
        #~ print('caught IOError')
    #~ except Exception as e:
        #~ print('caught other exception:', e)
    #~ return

    s = r.bundles
    print(type(s))
    #~ print(s.encode('utf8'))
    print(repr(s).encode('utf8'))
    #~ for row in r.bundles:
        #~ print('row:%s' % row)
        #~ print('MANIFEST:')
        #~ print(row.get_unsigned_manifest)
        #~ for k in row.keys():
            #~ print('%s : %s' % (k, row[k]))

if __name__ == '__main__':
    main()
